import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

import 'alertifyjs/build/css/alertify.min.css';
import 'alertifyjs/build/css/themes/default.min.css';
import alertify from 'alertifyjs';


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    profileArray: [],
    selectedProfile: {
      serverName: "realtime",
      displayName: "זמן אמת",
      ipAddress: "localhost",
      port: 9090
      },
    userArray: []
  },
  mutations: {
    profileSetter(state, profiles) {
      state.profileArray = profiles;
    },
    selectedProfileSetter(state, profile) {
      state.selectedProfile = profile;
      axios.get(`http://${state.selectedProfile.ipAddress}:${state.selectedProfile.port}/users`).then(response => {
        state.userArray = response.data;
        alertify.set('notifier','position', 'bottom-left');
        alertify.success('Switched Databases successfully!');
      })
      .catch(error => {
        alertify.set('notifier','position', 'bottom-left');
        alertify.error('Oops, there was an error');
      });
    },
    usersSetter(state, users) {
      state.userArray = users;
    }
  },
  actions: {
    setProfiles(state, profiles) {
      state.commit("profileSetter", profiles);
    },
    setSelectedProfile(state, profile) {
      state.commit("selectedProfileSetter", profile);
      
    },
    setUsers(state, users) {
      state.commit("usersSetter", users);
    }
  }
})
